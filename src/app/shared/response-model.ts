
export interface ResponseModel {
  code?: string;
  message: string;
  data: any;
  meta?: MetaModal;
  errors?: [];
}

export interface MetaModal {
  pagination: PaginationModal;
}

export interface PaginationModal {
  currentPage: number;
  firstPage: number;
  lastPage: number;
  perPage: number;
  count: number;
  totalRecords: number;
  links: Links;
}

export interface Links {
  first: string;
  last: string;
  previous?: string;
  next: string;
}


export interface AuthModel {
  tokenType: string;
  expiresIn: number;
  accessToken: string;
  refreshToken: string;
  userId: string;
}
