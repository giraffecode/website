import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ifa-website';

  constructor(private route: Router, private spinner: NgxSpinnerService ) {
  }

  ngOnInit() {
    this.route.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.spinner.show();
        }
        else if (event instanceof NavigationEnd) {
          this.spinner.hide();
        }
      },
      error => {
        this.spinner.hide();
      })
  }
}
