import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../shared/response-model';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) {}

  public contact(
    data
  ): Observable<ResponseModel> {

    let URL =  `${environment.apiUrl}/contacts`

    return this.http.post<ResponseModel>(URL, data);
  }

  public message(
    data
  ): Observable<ResponseModel> {

    let URL =  `${environment.apiUrl}/messages`;;

    return this.http.post<ResponseModel>(URL, data);
  }

}
