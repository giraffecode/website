import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../shared/response-model';

@Injectable({
  providedIn: 'root'
})
export class ProgramsService {

  URL = `${environment.apiUrl}/programmes`;

  constructor(private http: HttpClient) {}

  public list(
    filterObject = {page: 0}
  ): Observable<ResponseModel> {

    let URL =  this.URL +'?';

    for (const key in filterObject) {
      if (Object.prototype.hasOwnProperty.call(filterObject, key)) {
        if (Array.isArray(filterObject[key])) {
          filterObject[key].forEach(element => {
            URL += `${key}[]=${element}&`;
          });
        }
        else{
          URL += `${key}=${filterObject[key]}&`;
        }
      }
    }
    return this.http.get<ResponseModel>(URL);
  }

}
