import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContactService } from 'src/app/services/contact.service';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  formGroup: FormGroup;
  loading$ = false;

  constructor(private fb: FormBuilder, public validation: ValidationService, private contactService: ContactService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      firstName: [, Validators.required],
      lastName: [, Validators.required],
      gender: ["", Validators.required],
      mobile: [, Validators.required],
      email: [, [Validators.required, Validators.email]],
      age: [, Validators.required],
      request: [, Validators.required],

    })
  }

  onSubmit(){
    this.loading$ = true
    this.contactService.contact(this.formGroup.value).subscribe(
      res=>{
        this.loading$ = false;
        this.toastr.success(res.message);
      },
      err=>{
        this.loading$ = false;
        this.toastr.error(err.error.message)
      }
    );
  }

}
