import { Component, OnInit } from '@angular/core';
import { ProgramsService } from 'src/app/services/programs.service';
// import Swiper core and required modules
import SwiperCore, { Pagination, Navigation } from "swiper/core";

// install Swiper modules
SwiperCore.use([Pagination, Navigation]);

@Component({
  selector: 'app-programs-list',
  templateUrl: './programs-list.component.html',
  styleUrls: ['./programs-list.component.scss']
})
export class ProgramsListComponent implements OnInit {

  programsList = [];

  constructor(private programsService: ProgramsService) { }

  ngOnInit(): void {
    this.programsService.list().subscribe( res =>
      this.programsList = res.data as any[]
    )
  }

}
