import { Component, OnInit } from '@angular/core';
import { SponsorService } from 'src/app/services/sponors.service';
import SwiperCore, { Pagination, Navigation } from "swiper/core";
SwiperCore.use([Pagination, Navigation]);
@Component({
  selector: 'app-sponser',
  templateUrl: './sponser.component.html',
  styleUrls: ['./sponser.component.scss']
})
export class SponserComponent implements OnInit {

  partnersList = [];

  constructor(private sponsorService: SponsorService) { }

  ngOnInit(): void {
    this.sponsorService.list().subscribe( res =>
      this.partnersList = res.data as any[]
    )
  }

}
