import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContactService } from 'src/app/services/contact.service';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  date = new Date();
  year: number | undefined;
  formGroup: FormGroup;
  loading$ = false;

  constructor(private fb: FormBuilder, public validation: ValidationService, private contactService: ContactService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.year = this.date.getFullYear()

    this.formGroup = this.fb.group({
      firstName: [, Validators.required],
      lastName: [, Validators.required],
      email: [, [Validators.required, Validators.email]],
      message: [, Validators.required],
    })
  }

  onSubmit(){
    this.loading$ = true
    this.contactService.message(this.formGroup.value).subscribe(
      res=>{
        this.loading$ = false;
        this.toastr.success(res.message);
      },
      err=>{
        this.loading$ = false;
        this.toastr.error(err.error.message)
      }
    );
  }

}
